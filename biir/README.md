![](biircode.svg)

Biir is a 3-bit version of PiRM!T that (in a colour) occupies and combines the beyond-ASCII RGB bit of each channel. Biir plasmes - known as _spirit_ - can be inserted into PiRMIT rifts and wrapped around Tetrons to achieve unpredictable dynamics.

The initial distribution of birs is set in red. Green and blue dictate the rules for upping and downing bir. A diagonal dotted line marks a ghost for what it is and carries rule flags at its center. The ratio between a sector's surface and the number of shiip it contains determines their pH.

The 3-bit spectrum of a single bir is referred to as a chir.

Birs in a booth draft bir and spin their records based on the atmosphere. When both booths contain bir, they have a scratch battle.

## Knupbelbir's egohoge Sesamstratosfeer

A biir console for spirit fabrication.

## ColoDoT: Return of the Knupbelbir

A BGR tri-colour turn-based roguelike adventure plasmespace explorer PiRMIT interface featuring 

- Wifi Wafer (B)
- Mitr'l D. Spinsiil (G)
- Veil Wordsmith (R)

When you die, the bridge of death determines as which character you respawn.









