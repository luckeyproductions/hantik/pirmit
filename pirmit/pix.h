#ifndef PIX_H
#define PIX_H

#include "stdlib.h"
#include "limits.h"
#include <string>

namespace pirmit {

enum dia{ R, G, B, ALPHA, ALL };
const unsigned char mirbit{ 0200 };
//const unsigned char rift{ 0600 };

using peer = std::pair<unsigned char, unsigned char>;

unsigned int rev(unsigned char& c)
{
    unsigned char r{ 0 };

    while (c > 0) { r <<= 1;
        if (c & 1 == 1)
            r ^= 1; c >>= 1; }
    c = r;
}

struct col {
    unsigned char r_;
    unsigned char g_;
    unsigned char b_;
    unsigned char a_;
};
struct loc {
    unsigned x_;
    unsigned y_;
};

struct pix {
    pix(): pow2base_{ 00 } {} // Assume color
    pix(unsigned char r = 00,
        unsigned char g = 00,
        unsigned char b = 00,
        unsigned char a = mirbit << 1,
        unsigned x = 00,
        unsigned y = 00):
        col_{ r, g, b, a },
        loc_{ x, y },
        pow2base_{ 03u } {}

    const unsigned pow2base_;
    col col_; loc loc_;

    void set(unsigned char c, dia d)
    {
        if (d > ALPHA)
            return;

        getC(d) = c;
    }

    void imbue(unsigned char c, dia d, bool mir)
    {
        unsigned char er{ 0177 };
        c &= er;

        if (mir)
        {
            rev(er);
            rev(c);
        }

        getC(d) &= ~er;
        getC(d) |= c;
    }

    unsigned char& getC(dia d)
    {
        switch (d) {
        case     R: return col_.r_;
        case     G: return col_.g_;
        case     B: return col_.b_;
        default:
        case ALPHA: return col_.a_;
        }
    }
};

#endif // PIX_H
