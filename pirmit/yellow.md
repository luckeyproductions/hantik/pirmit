![](yellow.svg)

# Yellow [PiRMIT](README.md)

_Welcome to Metal-Art_


The commonalities of [Red](red.md), [Green](green.md) and [Blue](blue.md) are described in this section. This includes things like numbers, transitions, defining objects and the selector syntax as well as the plasmes format.

## Numbers


In most cases, numbers and lower case letters are interpreted as normalized octal fractions:

 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | ^
---|---|---|---|---|---|---|---|---
 a | b | c | d | e | f | g | h | ^
0.0|0.125|0.25|0.375|0.5|0.625|0.75|0.875|1.0

 01 | 02 | 03 | 04 | 05 | 06 | 07 | 10
----|----|----|----|----|----|----|---
 ab | ac | ad | ae | af | ag | ah | ba
0.015625|0.03125|0.046875|0.0625|0.078125|0.09375|0.109375|0.125

Plane coordinates and Pi radians:

 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | ^
---|---|---|---|---|---|---|---|---
 a | b | c | d | e | f | g | h | ^
-1|-3/4|-1/2|-1/4|0|1/4|1/2|3/4|1
-1.0|-0.75|-0.5|-0.25|0|0.25|0.5|0.75|1.0

Tau radians:

 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | ^
---|---|---|---|---|---|---|---|---
 a | b | c | d | e | f | g | h | ^
-1/2|-3/8|-1/4|-1/8|0|1/8|1/4|3/8|1/2
-0.5|-0.375|-0.25|-0.125|0.0|0.125|0.25|0.375|0.5

In case of a direction, it is interpreted as part of a circle. Often, the default value is 4-ward.

[![৪](https://weergeencookies.nl/four.svg)](https://en.wiktionary.org/wiki/%E0%A7%AA)

## Symbols

 Glyph | Yellow | Red | Green | Blue | Alpha
 -------|--------|-----|-------|-----|--------
**`[0-^]`** | | Snotnumbers
**`1d\`** | 1 3/8 | ||Octave|
 **`{`**|Definition|Form/Structure|Input/Output|State/Keyframe
 **`<`**|Transition|Curve| `<   =>` |
 **`z`** | Triangular |  | |
 **`8`** | 8-ball | Yes | Casting dice | Yes
 **`9`** | Yellow call | GiReen Call | Check mirror (2 mort) | Ring green
**`q`** | Quadrant multiplier| Required quality |Put cigar| Que |Cigar &&
**`k`** | Eris | x R-Mirbit  / Speaker | x G-Mirbit | Cut+<8>
**`e`** | | Ear
 **`@`** | Raven |`0 - 63`, moves bricks |Follows one side, returns another and randomizes |Interpolates, stays away from RG 
  **`&`** | String insertion | Mouth | Scroll to key | | Named typed string
**`p`** | Constant precision Input | Present | Put | Constant precision Output
**`x`** | Execute | | Marks the spot |
**`z`** | Switch | Track:Trigger | Scribe |
**`zzz`** _(ortho)_ | Sleep
**`#`** | Fence || Swam p!ramid || URL
**`=`** | Tape | Connects 
**`!`** | Bell, puts bir |
**`y`** | Color

## Transition



Special |[]()
---|---
**`-=B=-`** | B-Side marker for text formats

## Colours

Colours are define in base4 RGB and base2 Alpha. To achieve _full_ colour, they can be lerped.

## Arrays

There are several array structures in PiRMIT.

### 8-Ball

The 8-ball accesses a random route from its green 4X pixels.

## Yellow calls

When placed at the beginning of a transition, it can be used as a starting point when triggered. Whereas the `9`s at the end of a transition serves as transmission.

# Cigars

Cigars are stored in mir corners of octal sectors and kept - as Cubins - in the cigarbox located in the bottom left corner.
The captain's cigars plays a special role as it can be used to open a mouth, functioning as keys to enter otherwise inaccessible paths, or even add rooms to the world.

## Plasmes

Plasmes are files that combine an entity's red, green and blue PiRMIT code into a pair of 32-bit rgba images. Each colour forms its own branched veins, starting from the top left corner. Parts of code are reused by means of octal pixel address references. These reused macros are stored along the sides, meaning one of their starting coordinates will always be 0 or ^ - 1.

### The Extent

An important sector of the plasme is the _extent_. This is a special space where few may enter, and different rules apply. It may serve many functions:

- Engagement sendbox
- Dicerolls (Green prime)
- Battleground (Red & Blue)
- Texture of the metromoon
- Boot DJ when party gets started
- Scales for things like arm wrestling
- Chess board
- Sabers
- Sponge
- Reaction chamber
- Two-side paper

The size (0-^) of the extent can vary between 1/64th and the full surface of a plasme. 

### Card

A snapshot can be taken at any moment. It is like a screenshot, but instead of saving a screenbuffer it packs the current _state_ of all present plasmes into one without noise mirroring or direct red-green.

### Spellboard

This persistent local plasme stores things like your respawn plasme and other non-entity-related summoning abilities.

### Barrels

Each plasma has a barrel to contain its unseen contents.

### Assumptions

Conditions | Implications
---|---
All red, no angular maxima | Static mesh

### Parrots

The blue parrot is sparked to engage a plasme, this does not mean it'll voom. The blue and white (mir A) collars  knows the last state blue head, as well as the green head's position. In case there is no green info on the blue parrot the plasme is considered dead and may be revived. The red parrot knows the lifetime of this plasme.

## Piram!ds

The (engine/environment agnostic) plasmes can be stacked into 3D images (_stories_), which - sorted by size - fit a pyramid. These constitute the current scene.

Piram!ds are stacked per six into a cube, each with it's own tip and beyond addresses. This makes for a total of eight directions to pick from. Green can swap them out with another image, all colors can access them.

By default the journey starts at 0a0, but in this cube you can acquire Borg dice. In some cases the coordinates are expected to attenuate a setting, for instance cube 6g6.

When Samson the zebradog encounters a portal, it sends a raven to call the mayor who has the ability to validate cigars using his top hat.

# Black Whole Sun

Located in the furthest corner of the plasme within the Alpha channel. This is the dark realm where the one ring must be return for ET to phone home.

## Time

Time is stored in the black whole of a plasmes as an octal float. The sun dial stores phone coordinates in the black mirror.

### Chrono

The Chrono subsystem synchronizes plasmes to their latest timeframe by fast forwarding the lagging ones.

## The Rift

The rift - or sesamstraatosfeer in Dutch - where the mirror biits rezide, is read out from both sides of the plasme simultaneously. When its alpha component is flipped, the whole peer is switched with it. This horizontal part of the plasme can con

### Angels

The slitspace is occupied by three angels; one for each dialect, that all read the slits around them. You could say they look at life a different way.

Together, the bits read by the angels form the butterfly.

#### Raven

The raven is like a black angel, it adds soul to the blues. You can't stop raven; in some cases it is like a poking plague doctor and morbit messenger.

## Coldrones

In the center are three Coldrones where we can brew _tehtriscolvos_. A single octal value - within the rooster rift - controls which one to drink from or mix into. (Each one has a banana through which it can be observed and filled. But you can also make Jayzus turn water into wine when he saves a slaughtered ungulator.)

### Memory triangle

Serves as Pixel RAM, keeps track of egregor subscriptions.

### Tearwater pot

Below the erlenmeyers is a big pot that catches blue rings. (Keeping it full, increases a creatures' strength, and in some even their base power level.)

# Collar dressing

Each collar (o:1,1) contains the current evaluations of each colour. It can be ribped for inspection and interpirmitation.
