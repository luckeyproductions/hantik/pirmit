![](green.svg)

# Green [PiRMIT](README.md)

\*.pig (latin)

Red calls and Blue rings Nine to add variation and to connect animations to dynamic geometry generation. It can also control an object's logic and add inputs for interactivity. It pulls the strings in several ways.

Unlike R&B, green uses it's own mapping and takes strings more literally. Instead of tendons, the capital letters can be mapped to aliases/locations of any  mes. In some sense serving as runes or sigils for spellcasting.

Green carries a chaosphere that lights the eight squares around Nine.

Instead of wrapping, green's trajectory is flipped, both cardinally _and_  bit mirrored. In more technical terms, it goes over the edge.

When green PiRMIT does not modify itself, it's just glass. Glass is convenient in that differences with the original will always be within predictable limits. Making green pudding and jello may result in dyeing.

Number | Meaning
---|---
 0 | Void
 1 | Path (preferred step)
 2 | Choice
 3 | Spell definition
 4 | Spell absorption
 5 | Telescope
 6 | Turn left (gets mirrored)
 7 | Alert
 8 | Spirit jolt (0-7 -> RGB in ghost)

 Symbol | Meaning
 ---|---
**`q`** | Put red
**`p`** | Put green
**`b`** | Put blue
**`t<`** | Lerp blue t
**`v`** | Blue track value
**`g`** | goto
**`9`** | Observe mirror
**`c`** | Litter box (forward diagonal cat)
**`s`** | Sow seed to make it rain


[![](https://upload.wikimedia.org/wikipedia/commons/1/11/Egyptian_-_Finger_Ring_with_a_Representation_of_Ptah_-_Walters_42387_-_Side_A.jpg)](https://en.wikipedia.org/wiki/List_of_Mesopotamian_deities#Triad_of_Heaven)