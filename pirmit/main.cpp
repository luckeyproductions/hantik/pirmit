#include <stdio.h>
#include <iostream>
#include <termios.h>
#include <signal.h>

#include "defs.h"

namespace pirmit {


class Lezer
{
public:
    virtual void feed(char c) = 0; //Burn to image
    virtual char look(Direction d) = 0; //Moves head and reads glyph
    virtual bool run(); // while run, run
    unsigned headX_{ 00 };
    unsigned tailX_{ 00 };
};

class Snail: public Lezer
{
public:
    void feed(char c) override // Add char to mouth
    {
        mouth_ += c;
        chew();
    }
    char look(Direction d) override {}; //Read adjacent glyph
protected:
    void bite(); // add char to mouth
    bool chew() // Check last char and update char preference
    {

    }
    void spit(); // Discard last char from mouth (on faulty char and backspace)
    void vomit(); // Empty gut (backspace with empty mouth)
    void swallow() // Mouth to gut (on correct return and taste change)
    {
        gut_ += mouth_;
        mouth_.clear();
    }
private:
    std::string defecate(); // Gut to plasma (Return with empty mouth)

    std::string mouth_;
    std::string gut_;
    Direction forward_{ D_E };
};

struct State
{
    class Group
    {
        class Tendon
        {
            class Neuron
            {
                float x_{ 0.5f };
            };
        };
    };
};

class Entity
{
    /// Red: Create neuron within tendons and groups
    void AddNeuron(char group, char tendon, char* red)
    {

    }

    /// Green

    /// Blue: Animation controller
    void AddTrack(float length = 1.0f)
    {

    }
    void Play(std::string blue, unsigned track = 0, bool looped = false)
    {

    }
};

void printBone()
{
    printf("X :");
    printf("\n");
    printf("X0:");
    printf("\n");
    printf("X^:");
}

void printDialect()
{
    printf("Taste set to ");
    switch (activeDialect_)
    {
    default:
    case RED:   printf("Red");   break;
    case GREEN: printf("Green"); break;
    case BLUE:  printf("Blue");  break;
    }
    printf("\n");
}
};

void exit(sig_atomic_t s)
{
   printf("\n");
   printf("Byebye! @y");
   printf("\n");
   exit(1);
}


int main(int argc, char** argv)
{
    signal (SIGINT, exit);

    struct termios t;
    tcgetattr(1, &t);
    t.c_lflag &= ~ICANON;
    tcsetattr(1, 0, &t);

    using namespace pirmit;

    if (argc > 3)
    {
        printf("Usage:\n");
        printf("pirmit -rgb [PiRMIT code]\n");
        return 1;
    }

    if (argc == 2 || argc == 3)
    {
        if (strcmp(argv[1], "-g") == 0)
        {
            activeDialect_ = GREEN;
        }
        else if (strcmp(argv[1], "-b") == 0)
        {
            activeDialect_ = BLUE;
        }
    }

    system("clear");

    printDialect();

    printf("Type to feed the snail!\n@");

    Snail snail{};
    char in{};

    while (in != 27)
    {
        in = std::cin.get();

        system("clear");
        if (in == 10)
        {
            system("clear");
            printf("  | ");
        } else {
            printf("%c | ", in);
        }

//        snail.feed(in);
        printf("Dec: %d Oct: %o", in, in);
        printf("\n\n@");
    }

    return 0;
}


