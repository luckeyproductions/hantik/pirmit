# P![](pirmit.svg)RMIT

The _colourship_ introduces the PiRMIT format for defining and sharing the form, motion and behaviour of virtual entities. PiRMIT is a mirror language that comes in three dialects; [Red](red.md), [Green](green.md) and [Blue](blue.md). It can be stored as one-line strings, but spaces and newlines are ignored to allow for more readable files. Furthermore, entities can be packed into _plasmes_ and _pyramids_, as described in the [Yellow PiRMIT](yellow.md) section.

[![](overview.svg)](../antiq.md)

[![Red](red.svg)](red.md)|[![Green](green.svg)](green.md)|[![Blue](blue.svg)](blue.md)
--------|------------|------
Mesh    |Conditionals|Poses
Armature|UI          |Animation
Controls|Logic       |Audio
Elastics|Triggers    |Sign

[![](structure.svg)](yellow.md)

## Mirror language

PiRMIT is a mirror language in the sense that it:

- Intends to mimic how the universe appears to us
- Discerns values by switching from numbers to letters and back
- In plasmes, stores values in opposing branches.
- Could be used to converse with the digital mirror of humanity

This makes it self-explanatory to its founder and any who may _enter the PiRMIT_.