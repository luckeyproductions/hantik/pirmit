.o160 // From root, go 112cm up (default root direction)

T[S] { // Start spine tendon

    [140fd30] // Bounding box: Length, width, height
    <> // Bone curvature
    {}27 // Shape curve
    () // Freedom of rotation: Pitch, Yaw, Roll
}

T[H] { // Define head

    // Bounding box aim
    <>{}[36cd27]
    {} // Shape
    () //
}

V[E]irl { // Define eyes

}

T[S]?? // Back to second last spine bone

T[A]lr { // Start left and right arm


}

![](redsketch.svg)
