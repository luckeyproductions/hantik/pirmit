# [PiRMIT](README.md) Conventions

## Mapping

### Red & Blue

#### Groups

Letter | Group
--------|-------
 **H**  | Hands
 **A**  | Arms
 **L**  | Legs
 **F**  | Feet
 **T**  | Torso
 **V**  | Face
 **W**  | World

##### Hands group
 
The first two pose variables for bone sets within the hands group should represent the stretching and spreading of fingers.
 
 Letter | Set
--------|--------
 **P**  | Pinkie
 **R**  | Ring finger
 **M**  | Middle finger
 **I**  | Index finger
 **T**  | Thumb
 **W**  | Wrist
 
##### Torso group

 Letter | Set
--------|--------
 **S**  | Spine
 **T**  | Tail
 **F**  | Fins

#### Subgroups

Spatial subgroups are mapped to an octree which is subdivided along each axis individually as required.

 Letter | Subgroup
--------|--------
 **.**  | Center `(4, 4)`
 **r**  | Right  `(5-^)`
 **l**  | Left `(0-3)`
 **m**  | Middle `(4)`
 **f**  | Front `(_, 5-^)`
 **b**  | Back `(_, 0-3)`
 **a**  | Top
 **g**  | Bottom
 **p**  | Primary
 **s**  | Secondary
 **i**  | Cameras

This way we can select the thumb on the right hand with `H[T]r`, all eyes with `W[]ir` and the desired field using octal coordinates.

### Type map

This is the default string to type mapping using in alpha/black PiRMIT

String | File
---|---
I |
V | *.mkv *.ogv *.mp4
W |
