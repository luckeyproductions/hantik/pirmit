![](red.svg)

# Red [PiRMIT](README.md)

\*.pir

This dialect is used to generate meshes, their armatures, connecting control tendons and elastic bone relations. In general terms it defines form and limits.
When no maximum value is provided, red assumes it to be equal to the minimum.
In its structure Red zig zags the mirroring as required to avoid existing paths.

Mars attacks on red alert.

## Shaping

Defining shapes happens by instancing bones along which transitions are projected (in polar and longitudinal directions). A vorticity factor can be used to twist a mesh along a bone's length or surface node's normal.

## Groups, Tendons and Collections

Before being able to apply poses, the bones of a model should be mapped. This happens by linking bones to tendons, using a PiRMIT mold. _Groups_ are used to organize tendons. Collections can be used to narrow down a selection, for instance to limit an effect to the left or right side of an armature when setting a pose.

### Mapping

The mapping of groups and tendons technically depends on the applied molds, but to make models and animations transferable, stick to the [conventional mapping](conventions.md#mapping).

## Molds

A mold is that part of an object's red code without which no blue code can be run. It contains information about how a model's bones should be mapped to group, tendon and subgroup identifiers as well as the ranges within their transform space on which pose variables are projected. By default the first three pose variables of each tendon are mapped to the pitch, yaw and roll of its bones.

### Before first `|`

When a pipe exists before the first `{`, the text before the `|` is interpreted as a namespace assignment.

### After first `|`

Character | Meaning
-----------|---------
 **`#`** | Include 
 **`A-Z[]`** | Group selector
 **`[A-Z]`** | Tendon selector
 **`{}`**    | Definition confines

#### As first codon letter

 Character | Meaning
-----------|---------
  **`r`**   | Rock; track effect from rift raven
  **`m`**   | Metromoon
 
#### Within `{}`
  
 Character | Meaning
-----------|---------
 **`!`**     | Nth in chain
 **`?`** | `!` from chain tip
 **`#`**     | Digit selector
 **`&`**     | Letter selector
 **`*`**    | Selector wildcard
 **`|`**    | Assignment terminator
 **`v`**   | Vital
 **`%`** | Fly, spawn trigger

##### After `|`

 Character | Meaning
-----------|---------
 **`:`**     | Range splitter
 **<>**    | Easing
 **o**     | Full circle
 **_**     | Leave value

#### Within `[]`

 **`v`**   | Relative to parent box length
 **`w`**   | Relative to parent box width
 **`h`**   | Relative to parent box height

#### Within `()`

Character | Meaning
-----------|---------
 **`[]`**  | Complex tendon-bone relation
 **`/`**   | Sector selector


Sample mold for two hands:

```
r { *.R }
l { *.L }

H[PRMI] {
  Pinkie#.&,
  RingFinger#.&,
  MiddleFinger#.&,
  IndexFinger#.& | 2eh }
H[PI]! { __ -10 10 }
H[RM]! { __ -5 5 }

H[T]{Thumb#.&|30:-5} !{_,-60:30} !!{10:0}
```

#### Complex relations

By default the consecutive tendon factors respectively control the pitch, yaw and roll of the bones within a chain. Complex relations provide control over the influence a factor has on a bone's transform.

### The Extent

Red's extent zone is occupied by a hostile gorilla that throws the bananas it receives to determine 

## Red Hamsters

This plasme is tied to a bone until it gives a green light. This could be picking something up or freeing a princess.

## Surface properties

On the created shapes, surface sectors can be defined and assigned properties, colours, branches and vorticity. 

[![](https://upload.wikimedia.org/wikipedia/commons/8/88/Jasper.pebble.600pix.bkg.jpg)](https://en.wikipedia.org/wiki/List_of_Mesopotamian_deities#Triad_of_Heaven)
