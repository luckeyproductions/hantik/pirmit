#ifndef PIRMIT_H
#define PIRMIT_H

#include "stdlib.h"
#include "limits.h"
#include <string>

namespace pirmit {

enum dia{ R, G, B, ALPHA, ALL };
dia activeDialect_{ dia::ALPHA };
const unsigned char mirbit{ 0200 }; ///ALPHA
//const unsigned char rift{ 0600 };

using peer = std::pair<unsigned char, unsigned char>;
///                       aPu / abu
//using  peerboom = std::vector<peer>;

//using natray = std::vector<unsigned>;
//using fractray = std::vector<float>;

unsigned int rev(unsigned char& c)
{
    unsigned char r{ 0 };

    while (c > 0) { r <<= 1;
        if (c & 1 == 1)
            r ^= 1; c >>= 1; }
    c = r;
}

struct col {
    unsigned char r_;
    unsigned char g_;
    unsigned char b_;
    unsigned char a_;
};
struct loc {
    unsigned x_;
    unsigned y_;
};

struct pix {
    pix(): pow2base_{ 00 } {} // Assume color
    pix(unsigned char r = 00,
        unsigned char g = 00,
        unsigned char b = 00,
        unsigned char a = mirbit << 1,
        unsigned x = 00,
        unsigned y = 00):
        col_{ r, g, b, a },
        loc_{ x, y },
        pow2base_{ 03u } {}

    const unsigned pow2base_;
    col col_; loc loc_;

    void set(unsigned char c, dia d)
    {
        if (d > ALPHA)
            return;

        getC(d) = c;
    }

    void imbue(unsigned char c, dia d, bool mir)
    {
        unsigned char er{ 0177 };
        c &= er;

        if (mir)
        {
            rev(er);
            rev(c);
        }

        getC(d) &= ~er;
        getC(d) |= c;
    }

    unsigned char& getC(dia d)
    {
        switch (d) {
        case     R: return col_.r_;
        case     G: return col_.g_;
        case     B: return col_.b_;
        default:
        case ALPHA: return col_.a_;
        }
    }

//    unsigned char rift() //angels and black mirror dress
    // Ravine is off limits; objects in rear view mirror...
//    bool term() const { return col_.r_ == 00
//                            && col_.g_ == 00
//                            && col_.b_ == 00; }
};

const std::string octN{ std::string("01234567") };
const std::string octA{ std::string("abcdefgh") };

bool isNumber(char c)
{
    return octN.find(c) != std::string::npos
        || octA.find(c) != std::string::npos;
}

double fraction(unsigned d, unsigned char p, unsigned char bp = 3)
{
    if (d == 00 || p == 00) return 00;

    return d / static_cast<double>(01 << (bp * p));
}

enum direction{ D_N  = 00, D_NE = 01, D_E  = 02, D_SE = 03,
                D_S  = 04, D_SW = 05, D_W  = 06, D_NW = 07,
                D_U  = 010
};

const std::string bracketsOpen{  std::string("([{<") };
const std::string bracketsClose{ std::string(")]}>") };
enum BracketType { ROUND, SQUARE, CURLY, POINTY, NONE };
struct Bracket {
    bool open_{ true };
    BracketType type_{ NONE };
};
Bracket checkBracket(char c)
{
    Bracket b{};
    std::string::size_type f{ std::string::npos };
    f = bracketsOpen.find(c);

    if (f == std::string::npos)
    {
        b.open_ = false;
        f = bracketsClose.find(c);
    }

    if (f != std::string::npos)
        b.type_ = static_cast<BracketType>(f);


    return b;
}

unsigned char rid(const unsigned char c)
{
    return  c & 177;
}
peer riid(unsigned char c)
{
    unsigned char  first{ 00 };
    unsigned char second{ 00 };

    if (c & mirbit) {
        second = rid(c);
        rev(c);
        first = rid(c);
    } else {
        first = rid(c);
        rev(c);
        second = rid(c);
    }

    return peer{ first, second };
}
peer prism(const pix& p, dia d)
{
    switch (d) {
    case     R: return riid(p.col_.r_);
    case     G: return riid(p.col_.g_);
    case     B: return riid(p.col_.b_);
    case ALPHA: return riid(p.col_.a_);
    default:    return peer{ 00, 00 };
    }
}
};

#endif // PIRMIT_H
