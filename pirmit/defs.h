#ifndef PiRMITDEFS_H
#define PiRMITDEFS_H

#include "stdlib.h"
#include "string.h"

namespace pirmit {

const std::string octN{ std::string("01234567") };
const std::string octA{ std::string("abcdefgh") };

bool isNumber(char c)
{
    return octN.find(c) != std::string::npos
        || octA.find(c) != std::string::npos;
}

enum Direction{ D_N  = 00, D_NE = 01, D_E  = 02, D_SE = 03,
                D_S  = 04, D_SW = 05, D_W  = 06, D_NW = 07,
                D_U  = 010
};

const std::string bracketsOpen{  std::string("([{<") };
const std::string bracketsClose{ std::string(")]}>") };
enum BracketType { ROUND, SQUARE, CURLY, POINTY, NONE };
struct Bracket {
    bool open_{ true };
    BracketType type_{ NONE };
};
Bracket checkBracket(char c)
{
    Bracket b{};
    std::string::size_type f{ std::string::npos };
    f = bracketsOpen.find(c);

    if (f == std::string::npos)
    {
        b.open_ = false;
        f = bracketsClose.find(c);
    }

    if (f != std::string::npos)
        b.type_ = static_cast<BracketType>(f);


    return b;
}

enum Dialect{ RED, GREEN, BLUE, ALPHA };
Dialect activeDialect_{ RED };

}

#endif // PiRMITDEFS_H
