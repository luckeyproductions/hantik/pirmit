![](blue.svg)

# Blue [PiRMIT](README.md)

\*.pib

The scapegoating of Bobbin manages tendon waves. The current mes and normalized progression of each track is stored in the mirror, these also serve as the defaults.

## Poses

The _pose form_ is interpreted as normalized values which are used to lerp between the extremes of the mold. A series of instructions wrapped in a grouping is called a _tik_. Tiks can be assigned aliases, be used as transition keyframes and combined using operators. Tiks are wrapped in a Mes, then validated and sanitized by the PiRMIT interpretor before they can be fed into a HantikController.

 Character | Meaning
-----------|---------
 **#\~**   | Combination average
 **#^**    | Combination max
 **#0**    | Combination min
 **A-Z[]** | Group selector
 **[A-Z]** | Set selector
 **`:`**   | Range
 **`'`**   | Subgroup index (`Hr'''` for third right hand)
 **`!`**   | Override Nth bone (`H[T!!!^]` stretches thumbs' third phalanx)
 **`?`**   | Override Nth bone from the tip
 **`<>`**  | Keyframe separator
 **`<-+>`** / **`<+->`** | Ease in and out (default)
 **`</>`** | Linear interpolation
 **`<->`** | Ease in
 **`<+>`** | Ease out
 **`<-1f+>`** | Easing tension
 **`<^%...%^>`** | Span
 **`%`** | Become pawn (own root)
 **`%`** | Only when pawn

 Operator | Function
----------|----------
 **`-`**  | Minimum
 **`+`**  | Maximum
 **`/`**  | Average
 **`*`**  | Multiply
 **`++`** | Add
 **`--`** | Subtract
 **`~`**  | Invert (difference with ^ for each value)
 **`:`**  | Clamp
 **`<>`** | Transition

Capital letters inside `[]` select the tendons to which the values that follow them apply to. These values are denoted in octal form and with arbitrary precision. Alternation between numbers and letters is used to keep the values apart, starting with numbers for the first value.

`:` can be used to define a range of flexibility. As in Python, ranges can be open-ended. The Hantik component's base pose/animation along with its tension and bounce factors would control relaxation within the range. Ranges can also be used to clamp a tik's values.

`<>` signifies a transition. An animation from a thumbs up gesture to an ILY sign could be written as `PI0a<>7hT^h`.

Gestures can be managed and shared in sets which can be selected using its identifier followed by `|`.  `ils|Hr$(A)<>$(B)<>$(C)` would spell out "ABC" in international sign on the right hand.
The [Mesdex](https://gitlab.com/Modanung/hantik/-/wikis/Mesdex) is a catalog of gesture sets in wiki form.

## Goats

Instead of indefinite looping, Goats can be inserted into Blue code for tracks to self-trigger. They can be slaughtered (first goat after the first Sun). Jayzus can save the water into wine, for the black angel to drink.

Goats can be given a bounce factor, known as the ungulator

## Twinsun

The master track takes the role of a planet. Light from the Suns rains down on its surface. Saturn eats his children the moment he sees them. The metromoon  can be assigned a tempo and is located in the blue layer of the _extent_.

## The Extent

Part of the a plasme's extent is a multi-coloured structure known as the Psilophone. It can be used to create music or have a character fittingly anticipate an incoming banana/onions.


[![](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Lapis-lazuli_hg.jpg/1242px-Lapis-lazuli_hg.jpg)](https://en.wikipedia.org/wiki/List_of_Mesopotamian_deities#Triad_of_Heaven)
