# ϴϱ

Next generation tetrahedral PiRMIT with base4 values, coordinates and face indices. ThetaRho combines the logic (?) of PiRM!T and B!ir. It merges Lilliputian mirroring and diagonals (vertices) into a single byte. It may be better suited for _physical_ sound and long hair.

The edges of a _tetron_'s faces will be equivalents of recursive horse jumps. Mad tea parties will allow for interesting recombination.




