#ifndef LUCKEYMATH_H
#define LUCKEYMATH_H

#include <math.h>
#include <cmath>
#include <vector>

#define UNIT 010

template <class T> T spokeToRad( const T& theta) { return theta * static_cast<T>(M_PI_4); }
template <class T> T spokeToDeg( const T& theta) { return theta * static_cast<T>(  45  ); }
template <class T> T spokeToGon( const T& theta) { return theta * static_cast<T>(  50  ); }
template <class T> T spokeToQArc(const T& theta) { return theta * static_cast<T>( 720  ); }

template <class T>
static T lerp(const T a, const T b, const float t)
{
    return a + t * (b - a);
}

template <class T>
static T cycle(T x, T min, T max)
{
    using namespace std;

    T res{ x };

    if (min > max)
        swap(min, max);

    T range{ max - min };

    if (x < min)
        res += range * ceil((min - x) / range);
    else if (x > max)
        res -= range * ceil((x - max) / range);

    return res;
}

template <class T>
static T lerpAngle(const T a, const T b, const T max, const float t)
{
    using namespace std;

    T aCycled{ cycle(a, 0.0f, max) };
    T bCycled{ cycle(b, 0.0f, max) };

    if (aCycled > bCycled)
        swap(aCycled, bCycled);

    if (bCycled - aCycled < aCycled - bCycled + max)
        return lerp(aCycled, bCycled, t);
    else
        return cycle(lerp(bCycled, aCycled + max, t), 0.0f, max);
}

template <class T>
static T lerpSpoke(const T a, const T b, const float t)
{
    return lerpAngle(a, b, static_cast<T>(UNIT), t);
}
template <class T>
static T lerpRad(const T a, const T b, const float t)
{
    return lerpAngle(a, b, spokeToRad<T>(UNIT), t);
}
template <class T>
static T lerpDeg(const T a, const T b, const float t)
{
    return lerpAngle(a, b, spokeToDeg<T>(UNIT), t);
}
template <class T>
static T lerpGon(const T a, const T b, const float t)
{
    return lerpAngle(a, b, spokeToGon<T>(UNIT), t);
}
template <class T>
static T lerpQArc(const T a, const T b, const float t)
{
    return lerpAngle(a, b, spokeToQArc<T>(UNIT), t);
}

static double sine(double x)
{
    x = cycle(x, -M_PI, M_PI);

    double sin{};

    if (x < 0.0f)
        sin = 1.27323954f * x + 0.405284735f * x * x;
    else
        sin = 1.27323954f * x - 0.405284735f * x * x;

    if (sin < 0)
        sin = 0.225f * (sin *-sin - sin) + sin;
    else
        sin = 0.225f * (sin * sin - sin) + sin;

    return sin;
}
static double cosine(double x)
{
    return sine(x + M_PI * 0.5f);
}

#endif // LUCKEYMATH_H
